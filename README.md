0. Use Test Driven Development practices.
1. Please review the code, add your comments to improve the code.
2. Add express to run this code locally.
3. Add a new service that can be accessed via REST URI /echo which takes a string as parameter and return a json containing the same string. For example:
/echo?q='hello world'   and it returns { q= 'hello world' } (This is just an example you can return any json containing the string.)
4. Ensure that there are adequate unit tests for the newly written code and for the express server.
5. Use Karma library for testing
6. If you run into any problem with Karma try to fix it or delete the part not working, be creative.
7. Ensure that there is 100% code coverage unit tests with Istanbul library for the newly written code.

Priority: Reflect well written unit testing and TDD.